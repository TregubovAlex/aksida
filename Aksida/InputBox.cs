﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aksida
{
    public partial class InputBox : Form
    {
        bool AllowNull;
        public string text { get { return textBox.Text; } }
        public bool IsCanceled { get { return _IsCanceled; } }
        private bool _IsCanceled;
        public InputBox(string _oldName, bool _allowNull = false, string _title = "Введите название:")
        {
            InitializeComponent();
            this.Text = _title;
            AllowNull = _allowNull;
            textBox.Text = _oldName;
            textBox.SelectAll();
        }
        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.textBox.Text == "" && !AllowNull)
            {
                MessageBox.Show("Не введено название");
                _IsCanceled = true;
            }
            else
            {
                _IsCanceled = false;
                this.Close();
            }
        }
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            _IsCanceled = true;
            this.Close();
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (this.textBox.Text == "")
                    {
                        buttonOK_Click(sender, e);
                        this.textBox.Text = "";
                    }
                    else
                        buttonOK_Click(sender, e);
                    break;
                case Keys.Escape:
                    buttonCancel_Click(sender, e);
                    break;
            }
        }
    }
}
