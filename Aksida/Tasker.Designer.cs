﻿namespace Aksida
{
    partial class Tasker
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Tasker));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAddOrder = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonAddCall = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabelComleted = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabelDayRecord = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabelComletedDay = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabelSuspended = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabelInWork = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonHistory = new System.Windows.Forms.ToolStripButton();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.AutoSize = false;
            this.toolStrip.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.toolStripButtonAddOrder,
            this.toolStripSeparator2,
            this.toolStripButtonAddCall,
            this.toolStripSeparator8,
            this.toolStripLabelComleted,
            this.toolStripSeparator7,
            this.toolStripLabelDayRecord,
            this.toolStripSeparator5,
            this.toolStripLabelComletedDay,
            this.toolStripSeparator3,
            this.toolStripLabelSuspended,
            this.toolStripSeparator4,
            this.toolStripLabelInWork,
            this.toolStripSeparator6,
            this.toolStripButtonHistory,
            this.toolStripSeparator9});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(983, 50);
            this.toolStrip.TabIndex = 1;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 50);
            // 
            // toolStripButtonAddOrder
            // 
            this.toolStripButtonAddOrder.BackColor = System.Drawing.Color.Transparent;
            this.toolStripButtonAddOrder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddOrder.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripButtonAddOrder.ForeColor = System.Drawing.Color.Lime;
            this.toolStripButtonAddOrder.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddOrder.Image")));
            this.toolStripButtonAddOrder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddOrder.Name = "toolStripButtonAddOrder";
            this.toolStripButtonAddOrder.Size = new System.Drawing.Size(163, 47);
            this.toolStripButtonAddOrder.Text = "Добавить заявку ...";
            this.toolStripButtonAddOrder.Click += new System.EventHandler(this.toolStripButtonAddOrder_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.AutoSize = false;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(3, 50);
            // 
            // toolStripButtonAddCall
            // 
            this.toolStripButtonAddCall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddCall.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripButtonAddCall.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.toolStripButtonAddCall.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddCall.Image")));
            this.toolStripButtonAddCall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddCall.Name = "toolStripButtonAddCall";
            this.toolStripButtonAddCall.Size = new System.Drawing.Size(166, 47);
            this.toolStripButtonAddCall.Text = "Добавить звонок ...";
            this.toolStripButtonAddCall.Click += new System.EventHandler(this.toolStripButtonAddCall_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.AutoSize = false;
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(3, 50);
            // 
            // toolStripLabelComleted
            // 
            this.toolStripLabelComleted.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabelComleted.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabelComleted.ForeColor = System.Drawing.Color.White;
            this.toolStripLabelComleted.Name = "toolStripLabelComleted";
            this.toolStripLabelComleted.Size = new System.Drawing.Size(122, 47);
            this.toolStripLabelComleted.Text = "всего завершено: XXX";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator7.AutoSize = false;
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(3, 50);
            // 
            // toolStripLabelDayRecord
            // 
            this.toolStripLabelDayRecord.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabelDayRecord.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabelDayRecord.ForeColor = System.Drawing.Color.White;
            this.toolStripLabelDayRecord.Name = "toolStripLabelDayRecord";
            this.toolStripLabelDayRecord.Size = new System.Drawing.Size(112, 47);
            this.toolStripLabelDayRecord.Text = "рекорд за день: XXX";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator5.AutoSize = false;
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(3, 50);
            // 
            // toolStripLabelComletedDay
            // 
            this.toolStripLabelComletedDay.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabelComletedDay.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabelComletedDay.ForeColor = System.Drawing.Color.White;
            this.toolStripLabelComletedDay.Name = "toolStripLabelComletedDay";
            this.toolStripLabelComletedDay.Size = new System.Drawing.Size(132, 47);
            this.toolStripLabelComletedDay.Text = "за день завершено: XXX";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator3.AutoSize = false;
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(3, 50);
            // 
            // toolStripLabelSuspended
            // 
            this.toolStripLabelSuspended.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabelSuspended.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabelSuspended.ForeColor = System.Drawing.Color.White;
            this.toolStripLabelSuspended.Name = "toolStripLabelSuspended";
            this.toolStripLabelSuspended.Size = new System.Drawing.Size(85, 47);
            this.toolStripLabelSuspended.Text = "отложено: ХХХ";
            this.toolStripLabelSuspended.Click += new System.EventHandler(this.toolStripLabelSuspended_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator4.AutoSize = false;
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(3, 50);
            // 
            // toolStripLabelInWork
            // 
            this.toolStripLabelInWork.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabelInWork.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabelInWork.ForeColor = System.Drawing.Color.White;
            this.toolStripLabelInWork.Name = "toolStripLabelInWork";
            this.toolStripLabelInWork.Size = new System.Drawing.Size(78, 47);
            this.toolStripLabelInWork.Text = "в работе: XXX";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator6.AutoSize = false;
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(3, 50);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(983, 569);
            this.panel1.TabIndex = 2;
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator9.AutoSize = false;
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(3, 50);
            // 
            // toolStripButtonHistory
            // 
            this.toolStripButtonHistory.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonHistory.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonHistory.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripButtonHistory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.toolStripButtonHistory.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHistory.Image")));
            this.toolStripButtonHistory.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHistory.Name = "toolStripButtonHistory";
            this.toolStripButtonHistory.Size = new System.Drawing.Size(76, 47);
            this.toolStripButtonHistory.Text = "История ...";
            this.toolStripButtonHistory.Click += new System.EventHandler(this.toolStripButtonHistory_Click);
            // 
            // Tasker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.BackgroundImage = global::Aksida.Properties.Resources.temnyy_pyatna_fon_tekstura_50611_1920x1080;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(983, 619);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Tasker";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tasker";
            this.Click += new System.EventHandler(this.Tasker_Click);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddOrder;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabelComletedDay;
        private System.Windows.Forms.ToolStripLabel toolStripLabelInWork;
        private System.Windows.Forms.ToolStripLabel toolStripLabelSuspended;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel toolStripLabelComleted;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel toolStripLabelDayRecord;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddCall;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton toolStripButtonHistory;

    }
}

