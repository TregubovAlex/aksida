﻿namespace Aksida
{
    partial class Task
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Task));
            this.orderstep5 = new System.Windows.Forms.Label();
            this.orderstep4 = new System.Windows.Forms.Label();
            this.orderstep3 = new System.Windows.Forms.Label();
            this.orderstep2 = new System.Windows.Forms.Label();
            this.orderstep1 = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelNumber = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.поднятьНаПервоеСвободноеМестоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.отложитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelDateTime = new System.Windows.Forms.Label();
            this.callstep1 = new System.Windows.Forms.Label();
            this.callstep3 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labelBorderLeft = new System.Windows.Forms.Label();
            this.labelBorderRight = new System.Windows.Forms.Label();
            this.labelBorderTop = new System.Windows.Forms.Label();
            this.labelBorderBottom = new System.Windows.Forms.Label();
            this.labelOrderComplete = new System.Windows.Forms.Label();
            this.labelCallComplete = new System.Windows.Forms.Label();
            this.callstep2 = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // orderstep5
            // 
            this.orderstep5.BackColor = System.Drawing.Color.Green;
            this.orderstep5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderstep5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.orderstep5.Location = new System.Drawing.Point(6, 167);
            this.orderstep5.Name = "orderstep5";
            this.orderstep5.Size = new System.Drawing.Size(211, 30);
            this.orderstep5.TabIndex = 0;
            this.orderstep5.Text = "АКТУАЛ <- работы,\r\nNo счетов, комменты";
            this.orderstep5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.orderstep5.Click += new System.EventHandler(this.step1_Click);
            // 
            // orderstep4
            // 
            this.orderstep4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.orderstep4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderstep4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.orderstep4.Location = new System.Drawing.Point(20, 137);
            this.orderstep4.Name = "orderstep4";
            this.orderstep4.Size = new System.Drawing.Size(185, 30);
            this.orderstep4.TabIndex = 0;
            this.orderstep4.Text = "Проверка: Тинькофф,\r\nкол-во в счете = по факту";
            this.orderstep4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.orderstep4.Click += new System.EventHandler(this.step1_Click);
            // 
            // orderstep3
            // 
            this.orderstep3.BackColor = System.Drawing.Color.Lime;
            this.orderstep3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderstep3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.orderstep3.Location = new System.Drawing.Point(34, 107);
            this.orderstep3.Name = "orderstep3";
            this.orderstep3.Size = new System.Drawing.Size(158, 30);
            this.orderstep3.TabIndex = 0;
            this.orderstep3.Text = "1С: доки, подпись, печать";
            this.orderstep3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.orderstep3.Click += new System.EventHandler(this.step1_Click);
            // 
            // orderstep2
            // 
            this.orderstep2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.orderstep2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderstep2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.orderstep2.Location = new System.Drawing.Point(47, 77);
            this.orderstep2.Name = "orderstep2";
            this.orderstep2.Size = new System.Drawing.Size(132, 30);
            this.orderstep2.TabIndex = 0;
            this.orderstep2.Text = "ЗАЯВКА -> ВОДИТЕЛЮ";
            this.orderstep2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.orderstep2.Click += new System.EventHandler(this.step1_Click);
            // 
            // orderstep1
            // 
            this.orderstep1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.orderstep1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderstep1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.orderstep1.Location = new System.Drawing.Point(57, 47);
            this.orderstep1.Name = "orderstep1";
            this.orderstep1.Size = new System.Drawing.Size(108, 30);
            this.orderstep1.TabIndex = 0;
            this.orderstep1.Text = "Согласование";
            this.orderstep1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.orderstep1.Click += new System.EventHandler(this.step1_Click);
            // 
            // labelName
            // 
            this.labelName.BackColor = System.Drawing.Color.Transparent;
            this.labelName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelName.ForeColor = System.Drawing.Color.White;
            this.labelName.Location = new System.Drawing.Point(2, 7);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(219, 18);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Название";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelName.Click += new System.EventHandler(this.Order_Click);
            this.labelName.DoubleClick += new System.EventHandler(this.labelName_DoubleClick);
            this.labelName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Task_MouseDown);
            this.labelName.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Task_MouseMove);
            this.labelName.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Task_MouseUp);
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.BackColor = System.Drawing.Color.Transparent;
            this.labelNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNumber.ForeColor = System.Drawing.Color.White;
            this.labelNumber.Location = new System.Drawing.Point(7, 29);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(31, 15);
            this.labelNumber.TabIndex = 0;
            this.labelNumber.Text = "XXX";
            this.labelNumber.Click += new System.EventHandler(this.Order_Click);
            this.labelNumber.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Task_MouseDown);
            this.labelNumber.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Task_MouseMove);
            this.labelNumber.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Task_MouseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поднятьНаПервоеСвободноеМестоToolStripMenuItem,
            this.toolStripSeparator1,
            this.отложитьToolStripMenuItem,
            this.удалитьToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(303, 76);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // поднятьНаПервоеСвободноеМестоToolStripMenuItem
            // 
            this.поднятьНаПервоеСвободноеМестоToolStripMenuItem.Name = "поднятьНаПервоеСвободноеМестоToolStripMenuItem";
            this.поднятьНаПервоеСвободноеМестоToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.поднятьНаПервоеСвободноеМестоToolStripMenuItem.Text = "Переместить на первое свободное место";
            this.поднятьНаПервоеСвободноеМестоToolStripMenuItem.Click += new System.EventHandler(this.buttonMoveUp_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(299, 6);
            // 
            // отложитьToolStripMenuItem
            // 
            this.отложитьToolStripMenuItem.Name = "отложитьToolStripMenuItem";
            this.отложитьToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.отложитьToolStripMenuItem.Text = "Отложить ...";
            this.отложитьToolStripMenuItem.Click += new System.EventHandler(this.buttonSuspend_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить ...";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // labelDateTime
            // 
            this.labelDateTime.BackColor = System.Drawing.Color.Transparent;
            this.labelDateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDateTime.ForeColor = System.Drawing.Color.White;
            this.labelDateTime.Location = new System.Drawing.Point(58, 29);
            this.labelDateTime.Name = "labelDateTime";
            this.labelDateTime.Size = new System.Drawing.Size(164, 15);
            this.labelDateTime.TabIndex = 0;
            this.labelDateTime.Text = "Внесена: 24.02.2017 09:56";
            this.labelDateTime.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelDateTime.Click += new System.EventHandler(this.Order_Click);
            this.labelDateTime.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Task_MouseDown);
            this.labelDateTime.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Task_MouseMove);
            this.labelDateTime.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Task_MouseUp);
            // 
            // callstep1
            // 
            this.callstep1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.callstep1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.callstep1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.callstep1.Location = new System.Drawing.Point(15, 48);
            this.callstep1.Name = "callstep1";
            this.callstep1.Size = new System.Drawing.Size(192, 90);
            this.callstep1.TabIndex = 7;
            this.callstep1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.callstep1.Visible = false;
            this.callstep1.Click += new System.EventHandler(this.step1_Click);
            // 
            // callstep3
            // 
            this.callstep3.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.callstep3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.callstep3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.callstep3.Location = new System.Drawing.Point(15, 168);
            this.callstep3.Name = "callstep3";
            this.callstep3.Size = new System.Drawing.Size(192, 30);
            this.callstep3.TabIndex = 8;
            this.callstep3.Text = "ЗВОНОК ВОДИТЕЛЮ";
            this.callstep3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.callstep3.Visible = false;
            this.callstep3.Click += new System.EventHandler(this.step1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // labelBorderLeft
            // 
            this.labelBorderLeft.BackColor = System.Drawing.Color.Silver;
            this.labelBorderLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBorderLeft.Location = new System.Drawing.Point(0, 0);
            this.labelBorderLeft.Name = "labelBorderLeft";
            this.labelBorderLeft.Size = new System.Drawing.Size(3, 205);
            this.labelBorderLeft.TabIndex = 7;
            this.labelBorderLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBorderLeft.Click += new System.EventHandler(this.Order_Click);
            this.labelBorderLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Task_MouseDown);
            this.labelBorderLeft.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Task_MouseMove);
            this.labelBorderLeft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Task_MouseUp);
            // 
            // labelBorderRight
            // 
            this.labelBorderRight.BackColor = System.Drawing.Color.Silver;
            this.labelBorderRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBorderRight.Location = new System.Drawing.Point(220, 0);
            this.labelBorderRight.Name = "labelBorderRight";
            this.labelBorderRight.Size = new System.Drawing.Size(5, 205);
            this.labelBorderRight.TabIndex = 7;
            this.labelBorderRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBorderRight.Click += new System.EventHandler(this.Order_Click);
            this.labelBorderRight.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Task_MouseDown);
            this.labelBorderRight.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Task_MouseMove);
            this.labelBorderRight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Task_MouseUp);
            // 
            // labelBorderTop
            // 
            this.labelBorderTop.BackColor = System.Drawing.Color.Silver;
            this.labelBorderTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBorderTop.Location = new System.Drawing.Point(0, 0);
            this.labelBorderTop.Name = "labelBorderTop";
            this.labelBorderTop.Size = new System.Drawing.Size(225, 3);
            this.labelBorderTop.TabIndex = 7;
            this.labelBorderTop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBorderTop.Click += new System.EventHandler(this.Order_Click);
            this.labelBorderTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Task_MouseDown);
            this.labelBorderTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Task_MouseMove);
            this.labelBorderTop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Task_MouseUp);
            // 
            // labelBorderBottom
            // 
            this.labelBorderBottom.BackColor = System.Drawing.Color.Silver;
            this.labelBorderBottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBorderBottom.Location = new System.Drawing.Point(0, 200);
            this.labelBorderBottom.Name = "labelBorderBottom";
            this.labelBorderBottom.Size = new System.Drawing.Size(225, 5);
            this.labelBorderBottom.TabIndex = 7;
            this.labelBorderBottom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBorderBottom.Click += new System.EventHandler(this.Order_Click);
            this.labelBorderBottom.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Task_MouseDown);
            this.labelBorderBottom.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Task_MouseMove);
            this.labelBorderBottom.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Task_MouseUp);
            // 
            // labelOrderComplete
            // 
            this.labelOrderComplete.BackColor = System.Drawing.Color.Transparent;
            this.labelOrderComplete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelOrderComplete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelOrderComplete.Image = global::Aksida.Properties.Resources.OrderComplete;
            this.labelOrderComplete.Location = new System.Drawing.Point(0, 0);
            this.labelOrderComplete.Name = "labelOrderComplete";
            this.labelOrderComplete.Size = new System.Drawing.Size(225, 205);
            this.labelOrderComplete.TabIndex = 0;
            this.labelOrderComplete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelOrderComplete.Visible = false;
            // 
            // labelCallComplete
            // 
            this.labelCallComplete.BackColor = System.Drawing.Color.Transparent;
            this.labelCallComplete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCallComplete.Image = ((System.Drawing.Image)(resources.GetObject("labelCallComplete.Image")));
            this.labelCallComplete.Location = new System.Drawing.Point(0, 0);
            this.labelCallComplete.Name = "labelCallComplete";
            this.labelCallComplete.Size = new System.Drawing.Size(225, 205);
            this.labelCallComplete.TabIndex = 9;
            this.labelCallComplete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCallComplete.Visible = false;
            // 
            // callstep2
            // 
            this.callstep2.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.callstep2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.callstep2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.callstep2.Location = new System.Drawing.Point(15, 138);
            this.callstep2.Name = "callstep2";
            this.callstep2.Size = new System.Drawing.Size(192, 30);
            this.callstep2.TabIndex = 8;
            this.callstep2.Text = "В ЗАЯВКУ";
            this.callstep2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.callstep2.Visible = false;
            this.callstep2.Click += new System.EventHandler(this.step1_Click);
            // 
            // Task
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.labelBorderBottom);
            this.Controls.Add(this.labelBorderTop);
            this.Controls.Add(this.labelBorderRight);
            this.Controls.Add(this.labelBorderLeft);
            this.Controls.Add(this.callstep1);
            this.Controls.Add(this.callstep2);
            this.Controls.Add(this.callstep3);
            this.Controls.Add(this.orderstep1);
            this.Controls.Add(this.orderstep2);
            this.Controls.Add(this.orderstep3);
            this.Controls.Add(this.orderstep4);
            this.Controls.Add(this.labelDateTime);
            this.Controls.Add(this.labelNumber);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.orderstep5);
            this.Controls.Add(this.labelOrderComplete);
            this.Controls.Add(this.labelCallComplete);
            this.Name = "Task";
            this.Size = new System.Drawing.Size(225, 205);
            this.Click += new System.EventHandler(this.Order_Click);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Task_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Task_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Task_MouseUp);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label orderstep5;
        private System.Windows.Forms.Label orderstep4;
        private System.Windows.Forms.Label orderstep3;
        private System.Windows.Forms.Label orderstep2;
        private System.Windows.Forms.Label orderstep1;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelNumber;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem отложитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.Label labelDateTime;
        private System.Windows.Forms.Label labelOrderComplete;
        private System.Windows.Forms.Label callstep1;
        private System.Windows.Forms.Label callstep3;
        private System.Windows.Forms.Label labelCallComplete;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem поднятьНаПервоеСвободноеМестоToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label labelBorderLeft;
        private System.Windows.Forms.Label labelBorderRight;
        private System.Windows.Forms.Label labelBorderTop;
        private System.Windows.Forms.Label labelBorderBottom;
        private System.Windows.Forms.Label callstep2;
    }
}
