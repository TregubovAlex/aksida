﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;


namespace Aksida
{
    static class Statics
    {
        public static Form MainForm;

        public static string dataFilePath = Environment.CurrentDirectory + "\\Tasker_Data.xml";//.xlsx";

        public static int taskerRows = 3;
        public static int taskerRowsMax = 50;
        public static int taskerColumns = 4;
        public static int taskerToolStripHeight = 50;

        public static int taskWidth = 225;//300;
        public static int taskHeight = 205;//273;
        public static Color taskUnFocusedBackColor = Color.Transparent;//Color.DarkGray;
        public static Color taskFocusedBackColor = Color.LightGray;//Color.LightGray;

        public static Color stepCompletedColor = Color.Silver;//Color.Transparent;
    }
}
