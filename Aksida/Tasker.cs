﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Aksida
{
    public partial class Tasker : Form
    {
        public static int _InWork = 0;
        public static int _Suspended = 0;
        public static int _Completed = 0;
        public static int _CompletedDay = 0;
        public static int _DayRecord = 0;
        static Control.ControlCollection coll;
        public static bool[,] IsBusy = new bool[Statics.taskerRowsMax, Statics.taskerColumns];
        public static XDocument xmlDoc = XDocument.Load(Statics.dataFilePath);
        public Tasker()
        {
            InitializeComponent();
            Statics.MainForm = this;
            coll = this.panel1.Controls;
            this.Width = Statics.taskWidth * Statics.taskerColumns + this.Width - this.ClientSize.Width + 19;
            this.Height = Statics.taskHeight * Statics.taskerRows + Statics.taskerToolStripHeight + this.Height - this.ClientSize.Height;
            for (int i = 0; i < Statics.taskerRowsMax; i++)
                for (int j = 0; j < Statics.taskerColumns; j++)
                    IsBusy[i, j] = false;
            _DayRecord = Int32.Parse(xmlDoc.Root.Elements("record").First().Attribute("current").Value);
            int row = 0;
            int column = 0;
            foreach (XElement elem in xmlDoc.Root.Elements("job"))
            {
                if (elem.Attribute("state").Value == "Отложена")
                    elem.Attribute("state").Value = "В работе";
                switch (elem.Attribute("state").Value)
                {
                    case "В работе":
                        _InWork++;
                        Task newTask = new Task(row, column, elem);
                        IsBusy[row, column] = true;
                        column++;
                        if (column == Statics.taskerColumns)
                        {
                            column = 0;
                            row++;
                        }
                        break;
                    case "Завершена":
                        _Completed++;
                        if (DateTime.Parse(elem.Attribute("created").Value).Date == DateTime.Now.Date)
                            _CompletedDay++;
                        break;
                }
            }
            Task tempTask = new Task();
            tempTask.UpdateResults();
            tempTask.Dispose();
            xmlDoc.Save(Statics.dataFilePath);
        }
        private void toolStripLabelSuspended_Click(object sender, EventArgs e)
        {
            if (_Suspended != 0)
                if (MessageBox.Show("Принять все отложенные в работу?",
                                    "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    _Suspended = 0;
                    foreach (XElement elem in xmlDoc.Root.Elements("job"))
                        if (elem.Attribute("state").Value == "Отложена")
                        {
                            elem.Attribute("state").Value = "В работе";
                            _InWork++;
                            bool flag = true;
                            for (int row = 0; row < Statics.taskerRowsMax && flag; row++)
                                for (int column = 0; column < Statics.taskerColumns; column++)
                                    if (IsBusy[row, column] == false && row < Statics.taskerRowsMax)
                                    {
                                        Task newTask = new Task(row, column, elem);
                                        IsBusy[row, column] = true;
                                        flag = false;
                                        break;
                                    }
                        }
                    xmlDoc.Save(Statics.dataFilePath);
                }
        }
        private void toolStripButtonAddOrder_Click(object sender, EventArgs e)
        { AddTask(TaskType.ORDER); }
        private void toolStripButtonAddCall_Click(object sender, EventArgs e)
        { AddTask(TaskType.CALL); }
        private void AddTask(TaskType _type)
        {
            InputBox input = new InputBox("");
            input.ShowDialog();
            if (input.IsCanceled)
                return;
            for (int i = 0; i < Statics.taskerRowsMax; i++)
                for (int j = 0; j < Statics.taskerColumns; j++)
                    if (IsBusy[i, j] == false)
                    {
                        int newNumber;
                        if (xmlDoc.Root.Elements("job").Count() > 0)
                            newNumber = xmlDoc.Root.Elements("job").Max(job => Int32.Parse(job.Attribute("num").Value)) + 1;
                        else
                            newNumber = 1;
                        string type;
                        if (_type == TaskType.ORDER)
                            type = "Заявка";
                        else //CALL
                            type = "Звонок";
                        DateTime createdDateTime = DateTime.Now;
                        XElement newJob = new XElement("job",
                            new XAttribute("num", newNumber),
                            new XAttribute("type", type),
                            new XAttribute("name", input.text),
                            new XAttribute("state", "В работе"),
                            new XAttribute("created", createdDateTime.ToString()),
                            new XAttribute("completed", ""),
                            new XAttribute("step1", "."),
                            new XAttribute("step2", "."),
                            new XAttribute("step3", "."),
                            new XAttribute("step4", "."),
                            new XAttribute("step5", "."),
                            new XAttribute("content", ""));
                        xmlDoc.Root.Add(newJob);
                        xmlDoc.Save(Statics.dataFilePath);
                        _InWork++;
                        Task newTask = new Task(i, j, newJob);
                        IsBusy[i, j] = true;
                        return;
                    }
        }
        public static void ResetFocuses()
        {
            foreach (Control ctrl in coll)
                if (ctrl is Task)
                    (ctrl as Task).IsFocused = false;
        }
        private void Tasker_Click(object sender, EventArgs e)
        {
            ResetFocuses();
        }
        private void toolStripButtonHistory_Click(object sender, EventArgs e)
        {
            Form history = new History();
            history.ShowDialog();
        }
    }
}