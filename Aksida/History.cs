﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aksida
{
    public partial class History : Form
    {
        public History()
        {
            InitializeComponent();
            DataTable dt = new DataTable();
            dt.Columns.Add("№", typeof(Int32));
            dt.Columns.Add("Тип", typeof(String));
            dt.Columns.Add("Название", typeof(String));
            dt.Columns.Add("Статус", typeof(String));
            dt.Columns.Add("Создание", typeof(String));
            dt.Columns.Add("Завершение", typeof(String));
            dt.Columns.Add("Содержание", typeof(String));
            DataRow newRow;
            foreach (XElement elem in Tasker.xmlDoc.Root.Elements("job"))
            {
                newRow = dt.NewRow();
                newRow["№"] = Int32.Parse(elem.Attribute("num").Value);
                newRow["Тип"] = elem.Attribute("type").Value;
                newRow["Название"] = elem.Attribute("name").Value;
                newRow["Статус"] = elem.Attribute("state").Value;
                newRow["Создание"] = elem.Attribute("created").Value;
                newRow["Завершение"] = elem.Attribute("completed").Value;
                newRow["Содержание"] = elem.Attribute("content").Value;
                dt.Rows.Add(newRow);
            }
            dataGridView1.DataSource = dt;
            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                if (col.Name != "Содержание")
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                else
                    col.Width = 300;
            }
        }
        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Статус")
                switch (dataGridView1[e.ColumnIndex, e.RowIndex].Value.ToString())
                {
                    case "Отложена":
                        dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Yellow; break;
                    case "Завершена":
                        dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.DarkGray; break;
                    case "Удалена":
                        dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.FromArgb(64, 64, 64); break;
                }
            else if (dataGridView1.Columns[e.ColumnIndex].Name == "Тип")
            {
                dataGridView1[e.ColumnIndex, e.RowIndex].Style.Font = new Font(dataGridView1.DefaultCellStyle.Font, FontStyle.Bold);
                if (dataGridView1[e.ColumnIndex, e.RowIndex].Value.ToString() == "Заявка")
                    dataGridView1[e.ColumnIndex, e.RowIndex].Style.ForeColor = Color.Green;
                else
                    dataGridView1[e.ColumnIndex, e.RowIndex].Style.ForeColor = Color.Blue;
            }
            else if (dataGridView1.Columns[e.ColumnIndex].Name == "Создание")
                if (DateTime.Now > DateTime.Parse(dataGridView1[e.ColumnIndex, e.RowIndex].Value.ToString()).AddDays(2) &&
                    (dataGridView1[dataGridView1.Columns["Статус"].Index, e.RowIndex].Value.ToString() == "В работе" ||
                     dataGridView1[dataGridView1.Columns["Статус"].Index, e.RowIndex].Value.ToString() == "Отложена"))
                    dataGridView1[e.ColumnIndex, e.RowIndex].Style.ForeColor = Color.Red;
                else
                    dataGridView1[e.ColumnIndex, e.RowIndex].Style.ForeColor = Color.Black;
        }
    }
}