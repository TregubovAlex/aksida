﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Aksida
{
    public enum TaskType
    {
        ORDER,
        CALL
    }
    public partial class Task : UserControl
    {
        public TaskType Type;
        public bool IsFocused
        {
            get { return _IsFocused; }
            set
            {
                _IsFocused = value;
                if (_IsFocused == true)
                {
                    this.BackColor = Statics.taskFocusedBackColor;
                    this.labelName.Font = new Font(labelName.Font, FontStyle.Bold);
                    this.labelName.ForeColor = Color.Black;
                    this.labelNumber.ForeColor = Color.Black;
                    if (_IsLongLife)
                        this.labelDateTime.ForeColor = Color.Red;
                    else
                        this.labelDateTime.ForeColor = Color.Black;

                }
                else
                {
                    this.BackColor = Statics.taskUnFocusedBackColor;
                    this.labelName.Font = new Font(labelName.Font, FontStyle.Regular);
                    this.labelName.ForeColor = Color.White;
                    this.labelNumber.ForeColor = Color.White;
                    if (_IsLongLife)
                        this.labelDateTime.ForeColor = Color.Red;
                    else
                        this.labelDateTime.ForeColor = Color.White;
                }
            }
        }
        private bool _IsFocused = false;
        private bool _IsLongLife = false;
        private string[,] NormalVisibility = new string[30, 2];

        public XElement Elem;
        public int Row;
        public int Column;
        private Color[] ownStepColors = new Color[10];
        public Task()
        {
        }
        public Task(int _row, int _column, XElement _elem)
        {
            InitializeComponent();
            Row = _row;
            Column = _column;
            Elem = _elem;
            this.Width = Statics.taskWidth;
            this.Height = Statics.taskHeight;
            this.Location = new Point(Column * Statics.taskWidth + 1, Row * Statics.taskHeight);
            labelName.Text = _elem.Attribute("name").Value;
            if (_elem.Attribute("type").Value == "Заявка")
                Type = TaskType.ORDER;
            else //"Звонок"
                Type = TaskType.CALL;
            labelNumber.Text = _elem.Attribute("num").Value;
            DateTime dateTimeCreated = DateTime.Parse(_elem.Attribute("created").Value);
            labelDateTime.Text = "Внесена: " + dateTimeCreated.ToShortDateString() + ", " + dateTimeCreated.ToShortTimeString();
            if (DateTime.Now > dateTimeCreated.AddDays(2))
                _IsLongLife = true;
            string step1 = _elem.Attribute("step1").Value;
            string step2 = _elem.Attribute("step2").Value;
            string step3 = _elem.Attribute("step3").Value;
            string step4 = _elem.Attribute("step4").Value;
            string step5 = _elem.Attribute("step5").Value;
            string[] steps = new string[] { "", step1, step2, step3, step4, step5 };
            callstep1.Text = _elem.Attribute("content").Value;
            if (Type == TaskType.ORDER)
            {
                foreach (Control ctrl in this.Controls)
                    if (ctrl.Name.Contains("orderstep"))
                    {
                        int stepNum = Convert.ToInt32(ctrl.Name.Substring(9));
                        ownStepColors[stepNum] = ctrl.BackColor;
                        if (steps[stepNum] == "Готов")
                            ctrl.BackColor = Statics.stepCompletedColor;
                    }
            }
            else //CALL
            {
                foreach (Control ctrl in this.Controls)
                    if (ctrl.Name.Contains("callstep"))
                    {
                        ctrl.Visible = true;
                        int stepNum = Convert.ToInt32(ctrl.Name.Substring(8));
                        ownStepColors[stepNum] = ctrl.BackColor;
                        if (steps[stepNum] == "Готов")
                            ctrl.BackColor = Statics.stepCompletedColor;
                    }
                foreach (Control ctrl in this.Controls)
                    if (ctrl.Name.Contains("orderstep"))
                        ctrl.Visible = false;
            }
            int currCtrl = 0;
            foreach (Control ctrl in this.Controls)
            {
                NormalVisibility[currCtrl, 0] = ctrl.Name;
                NormalVisibility[currCtrl, 1] = ctrl.Visible.ToString();
                currCtrl++;
            }
            Tasker.ResetFocuses();
            IsFocused = true;
            Panel panel = (Panel)Statics.MainForm.Controls.Find("panel1", true).First();
            panel.Controls.Add(this);
            //Statics.MainForm.Controls.Add(this);
            UpdateResults();
        }
        private void Order_Click(object sender, EventArgs e)
        {
            Tasker.ResetFocuses();
            IsFocused = true;
        }
        private void labelName_DoubleClick(object sender, EventArgs e)
        {
            Tasker.ResetFocuses();
            IsFocused = true;
            InputBox input = new InputBox(labelName.Text);
            input.ShowDialog();
            if (input.IsCanceled)
                return;
            labelName.Text = input.text;
            Elem.Attribute("name").Value = input.text;
            Tasker.xmlDoc.Save(Statics.dataFilePath);
        }
        private void step1_Click(object sender, EventArgs e)
        {
            Label label = sender as Label;
            int stepNum = Convert.ToInt32(label.Name.Substring(label.Name.IndexOf('p') + 1));
            Color ownColor = ownStepColors[stepNum];
            Tasker.ResetFocuses();
            IsFocused = true;
            if (Type == TaskType.CALL && stepNum == 1)
            {
                InputBox input = new InputBox(label.Text, true, "Введите содержание звонка:");
                input.ShowDialog();
                if (input.IsCanceled)
                    return;
                label.Text = input.text;
                Elem.Attribute("content").Value = input.text;
                if (label.Text != "")
                {
                    label.BackColor = Statics.stepCompletedColor;
                    Elem.Attribute("step" + stepNum.ToString()).Value = "Готов";
                }
                else
                {
                    label.BackColor = ownColor;
                    Elem.Attribute("step" + stepNum.ToString()).Value = ".";
                }
            }
            else
            {
                if (label.BackColor != Statics.stepCompletedColor)
                {
                    label.BackColor = Statics.stepCompletedColor;
                    Elem.Attribute("step" + stepNum.ToString()).Value = "Готов";
                }
                else
                {
                    label.BackColor = ownColor;
                    Elem.Attribute("step" + stepNum.ToString()).Value = ".";
                }
            }
            Tasker.xmlDoc.Save(Statics.dataFilePath);
            foreach (Control ctrl in this.Controls)
                if (ctrl.Name.Contains("step") && ctrl.Visible == true)
                    if (ctrl.BackColor != Statics.stepCompletedColor)
                        return;
            this.BackColor = Color.Transparent; //TASK COMPLETED
            if (Type == TaskType.ORDER)
            {
                labelOrderComplete.BringToFront();
                labelOrderComplete.Visible = true;
            }
            else //CALL
            {
                labelCallComplete.BringToFront();
                labelCallComplete.Visible = true;
            }
            Elem.Attribute("state").Value = "Завершена";
            Elem.Attribute("completed").Value = DateTime.Now.ToString();
            Tasker._InWork--;
            Tasker._Completed++;
            Tasker._CompletedDay++;
            if (Tasker._CompletedDay > Tasker._DayRecord)
            {
                Tasker._DayRecord = Tasker._CompletedDay;
                Tasker.xmlDoc.Root.Elements("record").First().Attribute("current").Value = Tasker._CompletedDay.ToString();
                UpdateResults();
            }
            Tasker.xmlDoc.Save(Statics.dataFilePath);
            UpdateResults();
            timer1.Start();
        }
        int ElapsedTime = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            ElapsedTime++;
            if (ElapsedTime == 2)
            {
                Tasker.IsBusy[Row, Column] = false;
                this.Dispose();
            }
        }
        private void buttonMoveUp_Click(object sender, EventArgs e)
        {
            this.Dispose();
            Tasker.IsBusy[this.Row, this.Column] = false;
            for (int row = 0; row < Statics.taskerRowsMax; row++)
                for (int column = 0; column < Statics.taskerColumns; column++)
                    if (Tasker.IsBusy[row, column] == false && row < Statics.taskerRowsMax)
                    {
                        Task newTask = new Task(row, column, this.Elem);
                        Tasker.IsBusy[row, column] = true;
                        return;
                    }
        }
        private void buttonSuspend_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Отложить заявку \" " + labelName.Text + " \" ?",
                               "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                TerribleMessage();
                Tasker._Suspended++;
                Tasker._InWork--;
                UpdateResults();
                Elem.Attribute("state").Value = "Отложена";
                Tasker.xmlDoc.Save(Statics.dataFilePath);
                Tasker.IsBusy[Row, Column] = false;
                this.Dispose();
            }
        }
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите УДАЛИТЬ заявку \" " + labelName.Text + " \" ?",
                                "УДАЛЕНИЕ ЗАЯВКИ", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                TerribleMessage();
                Tasker._InWork--;
                UpdateResults();
                Elem.Attribute("state").Value = "Удалена";
                Elem.Attribute("completed").Value = DateTime.Now.ToString();
                Tasker.xmlDoc.Save(Statics.dataFilePath);
                Tasker.IsBusy[Row, Column] = false;
                this.Dispose();
            }
        }
        private void TerribleMessage()
        { MessageBox.Show("В ЗАЯВКУ !!!\n\n" + labelName.Text, "ВНИМАНИЕ", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
        private ToolStripLabel FindToolStripLabel(string _name)
        { return (ToolStripLabel)((ToolStrip)Statics.MainForm.Controls.Find("toolStrip", true)[0]).Items.Find(_name, true)[0]; }
        public void UpdateResults()
        {
            FindToolStripLabel("toolStripLabelInWork").Text = "в работе: " + Tasker._InWork.ToString();
            FindToolStripLabel("toolStripLabelSuspended").Text = "отложено: " + Tasker._Suspended.ToString();
            if (Tasker._Suspended != 0)
            {
                FindToolStripLabel("toolStripLabelSuspended").ForeColor = Color.Yellow;
                FindToolStripLabel("toolStripLabelSuspended").Font = new Font(FindToolStripLabel("toolStripLabelSuspended").Font, FontStyle.Bold | FontStyle.Underline);
            }
            else
            {
                FindToolStripLabel("toolStripLabelSuspended").ForeColor = Color.White;
                FindToolStripLabel("toolStripLabelSuspended").Font = new Font(FindToolStripLabel("toolStripLabelSuspended").Font, FontStyle.Regular);
            }
            FindToolStripLabel("toolStripLabelComletedDay").Text = "за день завершено: " + Tasker._CompletedDay.ToString();
            FindToolStripLabel("toolStripLabelDayRecord").Text = "рекорд за день: " + Tasker._DayRecord.ToString();
            if (Tasker._CompletedDay == Tasker._DayRecord)
            {
                FindToolStripLabel("toolStripLabelComletedDay").ForeColor = Color.Red;
                FindToolStripLabel("toolStripLabelDayRecord").ForeColor = Color.Red;
                FindToolStripLabel("toolStripLabelComletedDay").Font = new Font(FindToolStripLabel("toolStripLabelDayRecord").Font, FontStyle.Bold);
                FindToolStripLabel("toolStripLabelDayRecord").Font = new Font(FindToolStripLabel("toolStripLabelDayRecord").Font, FontStyle.Bold);
            }
            else
            {
                FindToolStripLabel("toolStripLabelComletedDay").ForeColor = Color.White;
                FindToolStripLabel("toolStripLabelDayRecord").ForeColor = Color.White;
                FindToolStripLabel("toolStripLabelComletedDay").Font = new Font(FindToolStripLabel("toolStripLabelDayRecord").Font, FontStyle.Regular);
                FindToolStripLabel("toolStripLabelDayRecord").Font = new Font(FindToolStripLabel("toolStripLabelDayRecord").Font, FontStyle.Regular);
            }
            FindToolStripLabel("toolStripLabelComleted").Text = "всего завершено: " + Tasker._Completed.ToString();
        }
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            Tasker.ResetFocuses();
            IsFocused = true;
        }

        private bool _IsMouseDown = false;
        private bool _WasMoved = false;
        private void Task_MouseDown(object sender, MouseEventArgs e)
        {
            _IsMouseDown = true;
            Tasker.ResetFocuses();
            IsFocused = true;
            this.BringToFront();
        }
        private void Task_MouseUp(object sender, MouseEventArgs e)
        {
            _IsMouseDown = false;
            if (_WasMoved)
            {
                int _newrow = Statics.MainForm.PointToClient(Control.MousePosition).Y / Statics.taskHeight;
                int _newcolumn = Statics.MainForm.PointToClient(Control.MousePosition).X / Statics.taskWidth;
                if (_newcolumn <= Statics.taskerColumns - 1)
                    if (Tasker.IsBusy[_newrow, _newcolumn] == false)
                    {
                        Tasker.IsBusy[Row, Column] = false;
                        Tasker.IsBusy[_newrow, _newcolumn] = true;
                        Row = _newrow;
                        Column = _newcolumn;
                    }
                this.Location = new Point(Column * Statics.taskWidth, Row * Statics.taskHeight);
                _WasMoved = false;
            }
        }
        int _oldNewRow;
        int _oldNewColumn;
        private void Task_MouseMove(object sender, MouseEventArgs e)
        {
            if (_IsMouseDown)
            {
                int _NewRow = Statics.MainForm.PointToClient(Control.MousePosition).Y / Statics.taskHeight;
                int _NewColumn = Statics.MainForm.PointToClient(Control.MousePosition).X / Statics.taskWidth;
                if (_NewColumn <= Statics.taskerColumns - 1)
                    if (_NewRow != _oldNewRow || _NewColumn != _oldNewColumn)
                    {
                        this.Location = new Point(_NewColumn * Statics.taskWidth, _NewRow * Statics.taskHeight);
                        _oldNewRow = _NewRow;
                        _oldNewColumn = _NewColumn;
                    }
                _WasMoved = true;
            }
        }
    }
}