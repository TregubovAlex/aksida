﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;

namespace Aksida
{
    class XML_Examples
    {
        string filename = "data.xml";
        int XMLRecord = 1;
        public XML_Examples()
        {
            //создание файла
            /*XDocument doc = new XDocument(
            new XElement("database",
                new XElement("record",
                    new XAttribute("Number", XMLRecord++),
                    new XAttribute("Name", "test333")),
                new XElement("record",
                    new XAttribute("Number", XMLRecord++),
                    new XAttribute("Name", "test777")),
                new XElement("record",
                    new XAttribute("Number", XMLRecord++),
                    new XAttribute("Name", "test999"))
                    ));
            doc.Save(filename);*/

            //чтение
            /*XDocument doc = XDocument.Load(filename);
            foreach (XElement elem in doc.Root.Elements())
            {
                Console.WriteLine("{0} {1}", elem.Name, elem.Attribute("Number").Value);
                //Console.WriteLine("  Attributes:");
                foreach (XAttribute attr in elem.Attributes())
                    Console.WriteLine("    {0}", attr);
                //Console.WriteLine("  Elements:");
                //foreach (XElement element in elem.Elements())
                //    Console.WriteLine("    {0}: {1}", element.Name, element.Value);
            }
            Console.ReadLine();*/

            //изменение
            /*XDocument doc = XDocument.Load(filename);
            foreach (XElement elem in doc.Root.Elements("record"))
            {
                int i = Int32.Parse(elem.Attribute("Number").Value);
                elem.SetAttributeValue("Number", ++i);
            }
            doc.Save(filename);*/

            //добавление
            /*XDocument doc = XDocument.Load(filename);
            int maxNumber = doc.Root.Elements("record").Max(r => Int32.Parse(r.Attribute("Number").Value));
            XElement record = new XElement("record",
                new XAttribute("Number", ++maxNumber),
                new XAttribute("Name", "test1212121212"));
            doc.Root.Add(record);
            doc.Save(filename);*/

            //удаление
            /*XDocument doc = XDocument.Load(filename);
            IEnumerable<XElement> records = doc.Root.Descendants("record").Where(r => r.Attribute("Name").Value.Contains("1"));
            records.Remove();
            doc.Save(filename);*/

            //запрос
            XDocument doc = XDocument.Load(filename);
            IEnumerable<XElement> records = from r in doc.Root.Elements("record")
                                            orderby r.Attribute("Number").Value ascending
                                            select r;
            /*foreach (XElement elem in records)
            {
                Console.WriteLine("{0} {1}", elem.Name, elem.Attribute("Number").Value);
                //Console.WriteLine("  Attributes:");
                foreach (XAttribute attr in elem.Attributes())
                    Console.WriteLine("    {0}", attr);
                //Console.WriteLine("  Elements:");
                //foreach (XElement element in elem.Elements())
                //    Console.WriteLine("    {0}: {1}", element.Name, element.Value);
            } 
            doc.Save(filename);*/
        }
    }
}